# 1ms latency : 4ms inter node
results/nts_lat_1 2 "delay 1ms"

# 2ms latency : 8ms inter node
results/nts_lat_2 2 "delay 2ms"

# 10ms latency : 40ms inter node
results/nts_lat_10 2 "delay 10ms"

# 100ms latency : 400ms inter node
results/nts_lat_100 2 "delay 100ms"
