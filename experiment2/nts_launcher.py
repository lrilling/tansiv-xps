#!/home/asrivastava/tansiv-xps/experiment2/venv/bin/python

#OAR -l nodes=1,walltime=01:00:00
#OAR -p cluster='grvingt'
#OAR -t besteffort
#OAR -t idempotent



# Invocation
# directory number options

from pathlib import Path
from subprocess import check_call
import sys

from tansiv.undercloud import localhost
from tansiv.api import start_nts, flent, dump, vm_emulate
from tansiv.constants import QEMU_IMAGE, DEFAULT_DOCKER_IMAGE

# give me the power
# this is required by the start_* function
check_call("sudo-g5k")

print(sys.argv)
_, directory, number, options = sys.argv

# fail fast if the directory exists
working_dir = Path(directory)
working_dir.mkdir(parents=True, exist_ok=True)


(working_dir / "cmd").write_text(" ".join(sys.argv))


roles, provider = localhost()
tansiv_roles, tansiv_networks = start_nts(roles, 
        int(number),
        "/home/asrivastava/python-tansiv/packer/packer-debian-10-x86_64-qemu/debian-10-x86_64.qcow2", 
        DEFAULT_DOCKER_IMAGE
)

vm_emulate(options, tansiv_roles, tansiv_networks)

for _ in range(10):
    flent(tansiv_roles, working_dir=working_dir, duration=60)

dump(roles, tansiv_roles, working_dir=working_dir)
